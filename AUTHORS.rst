=======
Credits
=======

.. _Zapis Dublya:

Development Team
----------------

* Vadim Andronov <vadimadr@gmail.com>
* Vladimir Zlobin <wovchena@gmail.com>
* Timofey Kuzmin <user@mail.ru>
* Alexander Tsyplyaev <alextsyplyaev@gmail.com>
* Mihail Dolinin <mixaildolinin@gmail.com>
