import Vue from 'vue';

import './index.scss';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

import App from './app/App.vue';
import Hello from './app/Main.vue';
import Settings from './app/Settings.vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: '/',
      components: {
        default: Hello
      },
      props: true
    },
    {
      path: '/settings',
      components: {
        default: Settings
      },
      props: true
    }
  ]
});

export default new Vue({
  el: '#root',
  router,
  render: h => h(App)
});
