===============================
IoT Light Control
===============================

.. image:: https://gitlab.com/zapis-dublya/iot-traffic-light-control/badges/master/build.svg
        :target: https://gitlab.com/zapis-dublya/iot-traffic-light-control/pipelines

.. image:: https://gitlab.com/zapis-dublya/iot-traffic-light-control/badges/master/coverage.svg
        :target: https://gitlab.com/zapis-dublya/iot-traffic-light-control/pipelines

Smart Traffic Light Controller based on Intel Edison


* Free software: MIT license
* Documentation: https://zapis-dublya.gitlab.io/iot-traffic-light-control


Features
--------

* TODO

Credits
-------
* `Zapis Dublya Team`_

.. _Zapis Dublya Team: AUTHORS.rst
