import os

_secret = os.environ.get('IOT_SECRET', 'dummy-secret')

_logging_config = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s %(levelname)s %(name)s - %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'default',
            'filename': 'logfile.log',
            'maxBytes': 1024,
            'backupCount': 3
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },

    },
    'root': {
        'handlers': ['console'],
        'level': 'INFO',
    },
    'loggers': {
        'iot_light': {
            'level': 'INFO'
        }
    }
}


class Config:
    STATIC_ROOT = '../webapp/dist'
    RESOLVER_URL = 'http://35.157.167.40:5050/edison'
    LED_RED_PIN = 8
    LED_YELLOW_PIN = 3
    LED_GREEN_PIN = 7

    SERVO_BARRIER_PIN = 5
    BUZZER_PIN = 6
    LCD_I2C = (0, 0x3E, 0x62)

    API_PORT = 8001

    BUZZER_VOLUME = .1

    DEFAULT_GREEN_DURATION = 20
    DEFAULT_RED_DURATION = 20

    LOGGING_CONFIG = _logging_config

    DATABASE_FILE = 'iottl_database.db'

    DEVELOP = False

    SECRET = _secret


class TestConfig(Config):
    LED_RED_PIN = 0
    LED_YELLOW_PIN = 0
    LED_GREEN_PIN = 0

    SERVO_BARRIER_PIN = 0
    BUZZER_PIN = 0

    DEVELOP = True


class ProductionConfig(Config):
    STATIC_ROOT = '/var/www'


if os.environ.get('IOT_SETTINGS_TEST'):
    config = TestConfig
elif os.environ.get('IOT_SETTINGS_PRODUCTION'):
    config = ProductionConfig
else:
    config = Config
