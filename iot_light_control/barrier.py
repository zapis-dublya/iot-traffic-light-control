import mraa


class ServoBarrier:
    def __init__(self, pin):
        self.pwm = mraa.Pwm(pin)
        self.period = round(1.0 / 50.0, 4)
        self.pwm.period(self.period)
        self.pwm.enable(True)
        self.angle = 90
        self.set_angle(90)

    def convert_angle(self, angle):
        return angle * (2500.0 - 550.0) / 180 + 550.

    def set_angle(self, angle):
        width = self.convert_angle(angle)
        self.pwm.write((width / 1000000) / self.period)
        self.angle = angle
