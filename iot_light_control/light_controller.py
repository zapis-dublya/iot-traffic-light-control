from asyncio import sleep

import asyncio

from logging import getLogger

import time
from upm import pyupm_i2clcd

from .video_analysis import TimeAdvicer
from .barrier import ServoBarrier
from .buzzer import Buzzer
from .led_light import LedLight
from .state import StateStore
from .config import config

logger = getLogger('iot_light.controller')


class TrafficLightController:
    def __init__(self):
        self.config = config
        self.state = StateStore()
        self.settings = StateStore()

        self.init_hardware()
        self.init_settings()
        self.init_state()
        self.init_listeners()

    def init_hardware(self):
        self.leds = {
            'green': LedLight(self.config.LED_GREEN_PIN),
            'yellow': LedLight(self.config.LED_YELLOW_PIN),
            'red': LedLight(self.config.LED_RED_PIN)
        }
        if not config.DEVELOP:
            self.lcd = pyupm_i2clcd.Jhd1313m1(*self.config.LCD_I2C)
            self.buzzer = Buzzer(self.config.BUZZER_PIN)
            self.servo = ServoBarrier(self.config.SERVO_BARRIER_PIN)
        self.cv_advicer = TimeAdvicer()
        cv_init = self.cv_advicer.test()
        if not cv_init:
            logger.error("Time Advicer test returns False")
        else:
            logger.info("Time Advicer initialized")

    def init_settings(self):
        settings = self.settings.state
        settings['red_duration'] = config.DEFAULT_RED_DURATION
        settings['green_duration'] = config.DEFAULT_GREEN_DURATION
        settings['smart_light'] = True

    def init_state(self):
        state = self.state.state
        state['green_active'] = False
        state['yellow_active'] = False
        state['red_active'] = False
        state['msg'] = ''
        state['buzzer_active'] = False
        state['barrier'] = 0
        state['road_situation'] = None

    def init_listeners(self):
        store = self.state
        store.subscribe('green_active', self.update_out)
        store.subscribe('red_active', self.update_out)
        store.subscribe('yellow_active', self.update_out)
        store.subscribe('buzzer_active', self.update_out)
        store.subscribe('msg', self.update_msg)
        store.subscribe('barrier', self.update_barrier)

    def update_out(self, key, value, old_value):
        logger.debug('%s changed from %s to %s' % (key, str(old_value),
                                                   str(value)))
        out_key = key[:key.find('_')]
        if out_key in ('red', 'green', 'yellow'):
            out = self.leds[out_key]
        elif out_key == 'buzzer':
            out = self.buzzer
        else:
            return

        if value:
            out.enable()
        else:
            out.disable()

    def update_msg(self, key, value, old_value):
        logger.debug('MSG: %s' % value)
        if len(value) > 16:
            msg1 = value[:16]
            msg2 = value[16:]
        else:
            msg1 = value
            msg2 = ''
        self.lcd.clear()
        self.lcd.setCursor(0, 0)
        self.lcd.write(msg1)
        self.lcd.setCursor(1, 0)
        self.lcd.write(msg2)

    def update_barrier(self, key, value, old_value):
        angle = (value * 90) / 100 + 90
        self.servo.set_angle(angle)

    async def run(self):
        state = self.state.state
        state['yellow_active'] = True
        green, red = self.update_period()
        state['yellow_active'] = False

        while True:
            await asyncio.wait([self.visual_indication('green', green),
                                self.sound_indication(green)])
            # YELLOW
            state['yellow_active'] = True
            state['msg'] = 'WARNING'
            t1 = time.perf_counter()
            green, red = self.update_period()
            elapsed = time.perf_counter() - t1
            if elapsed < 3:
                await sleep(3 - elapsed)
            state['yellow_active'] = False
            # RED
            await self.visual_indication('red', green)

    def update_period(self):
        settings = self.settings.state

        if settings['smart_light']:
            return self.advice_time()
        return settings['green_duration'], settings['red_duration']

    async def visual_indication(self, signal, signal_time):
        logger.debug('VISUAL INDICATION')
        state = self.state.state
        signal_green_ = signal == 'green'
        if signal_green_:
            real_time = signal_time - 4
        else:
            real_time = signal_time

        if signal_green_:
            msg = 'GO %d'
        else:
            msg = 'STOP %d'

        state['%s_active' % signal] = True
        for i in range(real_time):
            state['msg'] = msg % int(signal_time - i)
            await sleep(1)

        if signal_green_:
            logger.debug('BLINKING')
            b = False
            for i in range(8):
                state['%s_active' % signal] = b
                state['msg'] = msg % (4 - i // 2)
                b = not b
                await sleep(0.5)
        state['%s_active' % signal] = False

    async def sound_indication(self, time):
        logger.debug('SOUND INDICATION')
        state = self.state.state
        time_ = 6
        t = 0

        long_pause = .5
        short_pause = .15
        long_play = .4
        short_play = .15

        while t < time_ - 4:
            state['buzzer_active'] = True
            await sleep(long_play)
            state['buzzer_active'] = False
            await sleep(long_pause)
            t += long_play + short_play

        while t < time_:
            state['buzzer_active'] = True
            await sleep(short_play)
            state['buzzer_active'] = False
            await sleep(short_pause)
            t += short_play + short_pause

    def advice_time(self):
        first, second = self.cv_advicer.getCarN()
        self.state.state['road_situation'] = (first, second)
        if first is None or second is None:
            return config.DEFAULT_GREEN_DURATION, config.DEFAULT_RED_DURATION

        return min(first * 5, 10), min(second * 5, 10)
