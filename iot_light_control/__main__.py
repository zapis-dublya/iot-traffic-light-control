import sys

from iot_light_control.app import main

if __name__ == '__main__':
    sys.exit(main())
