import hashlib
from datetime import datetime

from sanic import Blueprint
from sanic.response import json
from itsdangerous import JSONWebSignatureSerializer as JWT

from .database import User, Sessions
from .config import config

api = Blueprint('api', url_prefix='api')


def verify_user(request):
    ts = request.app.token_storage

    token = request.headers.get('X-Auth-Token')

    if not token:
        token = request.json['token']

    return ts[token]


@api.route("/settings", methods=('GET', 'PUT'))
async def settings(request):
    user = verify_user(request)
    app = request.app.main_app
    settings = app.settings.state
    if request.method == 'GET':
        return json({
            "settings": {
                "smartLight": settings['smart_light'],
                "barrierHeight": user.barrier_height,
                "greenDuration": settings['green_duration'],
                "redDuration": settings['red_duration']
            }
        })

    if request.method == 'PUT':
        new_settings = request.json

        user.barrier_height = int(new_settings['barrierHeight'])
        user.save()

        settings['red_duration'] = int(new_settings['redDuration'])
        settings['green_duration'] = int(new_settings['greenDuration'])
        settings['smart_light'] = new_settings['smartLight']

        return json({"status": "ok"})


@api.route("/test")
async def api_test(request):
    return json({'status': 'ok'})


def api_error(msg):
    return json({"error": msg}, 400)


def create_token(user, ts):
    jwt = JWT(config.SECRET)
    timestamp = str(datetime.now().timestamp())
    token = jwt.dumps({'u': user.username, 't': timestamp}).decode()

    ts[token] = user

    Sessions.create(user=user, token=token)

    return json({"token": token})


@api.route("/login", methods=('GET', 'POST'))
async def api_login(request):
    token_storage = request.app.token_storage
    if request.method == 'GET':
        return json({"status": "ok"})

    if request.method == 'POST':
        username = request.json.get('username')
        password = request.json.get('password')

        if username is None or password is None:
            return api_error("Invalid data")

        password_hash = hashlib.sha1(password.encode()).hexdigest()

        s = User.select().where(User.username == username)

        if s.exists():
            user = s.first()

            if user.password_hash == password_hash:
                return create_token(user, token_storage)

        return api_error("Invalid user or password")


@api.route("/register", methods=('POST',))
async def api_register(request):
    username = request.json.get('username')
    password = request.json.get('password')

    if username is None or password is None:
        return api_error("Invalid data")

    if len(username) == 0:
        return api_error("Username is too short")

    if len(password) < 4:
        return api_error("Password should be longer than 4 symbols")

    s = User.select().where(User.username == username)

    if s.exists():
        return api_error("User already exists")

    password_hash = hashlib.sha1(password.encode()).hexdigest()

    new_user = User.create(username=username, password_hash=password_hash)

    return create_token(new_user, request.app.token_storage)


@api.route('/barrier', methods=('GET', 'PUT'))
async def barrier(request):
    app = request.app.main_app
    user = verify_user(request)
    barrier_open = app.state.state.get('barrier', 0) != 0
    if request.method == 'GET':
        return json({"barrierOpen": barrier_open})

    if request.method == 'PUT':
        if not barrier_open:
            app.state.state['barrier'] = user.barrier_height
        else:
            app.state.state['barrier'] = 0
        return json({"status": "ok"})


@api.route('/status')
async def road_status(request):
    app = request.app.main_app
    situation_ = app.state.state['road_situation']
    if situation_ is None:
        return json({"status": -1})
    if None in situation_:
        return json({"status": -2})
    return json({"status": 0, "first": situation_[0], "second": situation_[1]})
