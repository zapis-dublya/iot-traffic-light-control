from peewee import *
from playhouse.sqlite_ext import SqliteExtDatabase

from iot_light_control.config import config

db = SqliteExtDatabase(config.DATABASE_FILE)


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    username = CharField(unique=True)
    password_hash = CharField()

    barrier_height = IntegerField(default=50)


class Sessions(BaseModel):
    user = ForeignKeyField(User, related_name='sessions')
    token = CharField(unique=True)
