import asyncio
import socket
import fcntl
import struct
from pathlib import Path
from signal import SIGINT, signal

import logging
import logging.config
import uvloop
import requests
from sanic import Sanic
from sanic.response import text
from sanic_cors import CORS

from .database import db, User, Sessions
from .config import config
from .api import api as api_blueprint
from .light_controller import TrafficLightController

logger = logging.getLogger('iot_light')


def create_sanic_app(main_app):
    app = Sanic(__name__)
    CORS(app)

    @app.middleware('request')
    async def print_on_request(request):
        if request.method == 'OPTIONS':
            resp = text('')
            resp.headers['Access-Control-Allow-Origin'] = '*'
            resp.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, ' \
                                                           'OPTIONS'
            resp.headers[
                'Access-Control-Allow-Headers'] = "accept, content-type, " \
                                                  "X-Auth-Token"
            return resp

    static_root = Path(config.STATIC_ROOT)

    app.token_storage = {
        o.token: o.user for o in Sessions.select()
    }
    app.main_app = main_app
    app.blueprint(api_blueprint)
    # app.static('/', str(static_root.joinpath('index.html').resolve()))
    app.static('/', str(static_root.absolute()))

    return app


def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])


def register_resolver():
    if not config.DEVELOP:
        ip = get_ip_address(b'wlan0')
    else:
        ip = 'localhost'

    edison_url = 'http://%s:%d' % (ip, config.API_PORT)

    r = requests.post(config.RESOLVER_URL, json={"url": edison_url})
    if r.json().get('status') == 'ok':
        logging.info('Url resolved as %s' % edison_url)
    else:
        logging.error('Url not resolved [%d] error!' % r.status_code)


def init_database():
    if not Path(config.DATABASE_FILE).exists():
        db.create_tables([User, Sessions])


def main():
    logging.config.dictConfig(config.LOGGING_CONFIG)
    light_controller = TrafficLightController()

    init_database()
    register_resolver()
    app = create_sanic_app(light_controller)

    asyncio.set_event_loop(uvloop.new_event_loop())
    server = app.create_server(host="0.0.0.0", port=8001)
    loop = asyncio.get_event_loop()
    asyncio.ensure_future(server)
    asyncio.ensure_future(light_controller.run())
    signal(SIGINT, lambda s, f: loop.stop())

    try:
        loop.run_forever()
    except:
        loop.stop()
