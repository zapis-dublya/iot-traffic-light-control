# -*- coding: utf-8 -*-
from collections import defaultdict


class State(dict):
    def __init__(self, store, *args, **kwargs):
        self._store = store
        super(State, self).__init__(*args, **kwargs)

    def __setitem__(self, key, value):
        old_value = self.get(key)
        if old_value != value:
            super(State, self).__setitem__(key, value)
            self._store.notify(key, value, old_value)


class StateStore(object):
    def __init__(self):
        self._state = State(self)
        self._observers = defaultdict(list)

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, new_state):
        old_state = self._state
        self._state = State(self)
        self._state.update(new_state)
        for k in new_state:
            old_val = old_state.get(k)
            if new_state[k] != old_val:
                self.notify(k, new_state[k], old_val)

    def notify(self, key, value, old_value):
        observers = self._observers.get(key)
        if not observers:
            return
        for obs in observers:
            obs(key, value, old_value)

    def subscribe(self, key, observer):
        self._observers[key].append(observer)
