# -*- coding: utf-8 -*-
import mraa


class LedLight(object):

    def __init__(self, pin):
        self.led = mraa.Gpio(pin)
        self.led.dir(mraa.DIR_OUT)
        self.state = False

    def toggle(self):
        self.state = not self.state
        self.led.write(int(self.state))

    def disable(self):
        self.state = False
        self.led.write(0)

    def enable(self):
        self.state = True
        self.led.write(1)
