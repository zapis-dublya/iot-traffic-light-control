import mraa

from .config import config


class Buzzer:
    def __init__(self, pin):
        self.buzzer = mraa.Pwm(pin)
        self.buzzer.enable(True)
        self.buzzer.write(0)
        self.volume = config.BUZZER_VOLUME

    def enable(self):
        self.buzzer.period_us(3800)
        self.buzzer.write(self.volume)

    def disable(self):
        self.buzzer.period_us(1)
        self.buzzer.write(0)
