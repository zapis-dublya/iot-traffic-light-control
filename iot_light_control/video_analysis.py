# TODO что если не запускали тест()
# TODO поиск кусков дороги без 25 пикселей
from imutils.perspective import four_point_transform
import imutils
import numpy as np
import cv2
from skimage import measure


class TimeAdvicer:
    """
    Main class for the car detection.
    Captures the road image and finds cars on it.
    """

    def __init__(self, camN=0):
        """

        :param camN: Number of device /dev/video*
        """
        self.cam = cv2.VideoCapture(camN)
        self.broken = False

    def test(self):
        """
        Detects the road and print it as two images.
        :return: True if the road has been detected. False otherwise.
        """
        try:
            # Road detection indicator
            flag = False
            # Capture the road image from the webcam
            ret, image = self.cam.read()
            # Get the objects' borders
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            blurred = cv2.GaussianBlur(gray, (5, 5), 0)
            edged = cv2.Canny(blurred, 50, 200, 255)

            # Find contours in the edge map, then sort them by their
            # Size in descending order
            cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)
            cnts = cnts[0] if imutils.is_cv2() else cnts[1]
            cnts = sorted(cnts, key=cv2.contourArea, reverse=True)

            # Set of contours' points
            displayCnt = None

            # Loop over the contours
            for c in cnts:
                # Approximate the contour
                peri = cv2.arcLength(c, True)
                approx = cv2.approxPolyDP(c, 0.02 * peri, True)

                # If the contour has four vertices, then we have found
                # the thermostat display
                if (len(approx) == 12) and (cv2.contourArea(c) >= 1000):
                    displayCnt = approx

                    # Debug mode
                    flag = True
                    break
            # X and Y of contour points
            roadx = []
            roady = []

            for i in displayCnt:
                roadx.append(i[0][0])
                roady.append(i[0][1])

            # Get the set of the contours' points
            pts = []
            for i in range(0, len(roadx)):
                pts.append([roadx[i], roady[i]])

            # Get the array with border numbers
            convexNums = grahamscan(pts)

            xFirst = convexNums[0]
            # Getting first half of the road

            firstHalfRoad = [point for point in convexNums if
                             (isNear((pts[point])[0], (pts[xFirst])[0]) or
                              isNear((pts[point])[1], (pts[xFirst])[1])) and
                             point != xFirst]

            firstRoad = [point for point in convexNums if
                         (isNear((pts[point])[0],
                                 (pts[firstHalfRoad[0]])[0]) or
                          isNear((pts[point])[1],
                                 (pts[firstHalfRoad[0]])[1])) and
                         point != firstHalfRoad[0]] + firstHalfRoad
            # Other half of the road
            secondRoad = [i for i in set(convexNums) - set(firstRoad)]

            # Get two rectangles for the road recognition
            localApproxFirstRoad = [[[]]]
            localApproxSecondRoad = [[[]]]
            for y in firstRoad:
                localApproxFirstRoad.append([[pts[y]]])
            localApproxFirstRoad.pop(0)
            for y in secondRoad:
                localApproxSecondRoad.append([[pts[y]]])
            localApproxSecondRoad.pop(0)

            self.coordsOfFirstRoad = np.array(localApproxFirstRoad)
            self.coordsOfSecondRoad = np.array(localApproxSecondRoad)

            return flag

        # Debug
        except Exception:
            self.broken = True
            return False

    def getCarN(self):
        """
        Detects objects on the road
        :return: The number of cars on both first and second roads as x,y
        """
        try:
            if self.broken:
                return None, None
            # Get the road images
            ret, image = self.cam.read()

            # Get the objects' borders
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            # Get the road images
            r1 = four_point_transform(gray, self.coordsOfFirstRoad.reshape(
                4, 2))
            # cv2.imwrite(self.file1, r1)
            r2 = four_point_transform(gray,
                                      self.coordsOfSecondRoad.reshape(4, 2))
            # cv2.imwrite(self.file2, r2)

            # Calculate the threshold
            mean = 0
            for i in r1:
                for j in range(0, len(i)):
                    mean += i[j]
            mean = mean / (len(r1) - 1) / (len(r1[0]) - 1)

            # Get the car imaga on the road
            blurred = cv2.GaussianBlur(r1, (11, 11), 0)

            thresh = \
                cv2.threshold(blurred, mean * 0.8, 255, cv2.THRESH_BINARY_INV)[
                    1]
            thresh = cv2.erode(thresh, None, iterations=2)
            thresh = cv2.dilate(thresh, None, iterations=4)
            labels1 = measure.label(thresh, neighbors=8, background=0,
                                    return_num=True)
            # cv2.imwrite(self.file1, thresh)

            # Same for the second road
            mean = 0
            for i in r2:
                for j in range(0, len(i)):
                    mean += i[j]
            mean = mean / (len(r2) - 1) / (len(r2[0]) - 1)
            blurred = cv2.GaussianBlur(r2, (11, 11), 0)

            thresh = \
                cv2.threshold(blurred, mean * 0.8, 255, cv2.THRESH_BINARY_INV)[
                    1]
            thresh = cv2.erode(thresh, None, iterations=2)
            thresh = cv2.dilate(thresh, None, iterations=4)
            labels2 = measure.label(thresh, neighbors=8, background=0,
                                    return_num=True)
            # cv2.imwrite(self.file2, thresh)
            # Return the number of connected components
            return labels1[1], labels2[1]
        except Exception:
            # Debug
            self.broken = True
            return None, None


def showMeYourWorld(camN=0, file="showMeYourWorld.jpg"):
    flag = False
    cam = cv2.VideoCapture(0)
    ret, image = cam.read()
    cam.release()
    cv2.imwrite(file, image)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    edged = cv2.Canny(blurred, 50, 200, 255)

    # Find contours in the edge map, then sort them by their
    # Size in descending order
    cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)

    # Loop over the contours
    for c in cnts:
        # Approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)

        # If the contour has four vertices, then we have found
        # the thermostat display
        if (len(approx) == 12) and (cv2.contourArea(c) >= 1000):
            return True
    return flag


def rotate(A, B, C):
    """
    Vectors rotation
    """
    return (B[0] - A[0]) * (C[1] - B[1]) - (B[1] - A[1]) * (C[0] - B[0])


def grahamscan(A):
    """
    Get the convex border of the road
    :param A: The array with the road points
    :return: The array with numbers of the border points
    """
    # The number of the road points
    n = len(A)
    # The array of points' number
    P = [i for i in range(0, n)]
    # Get the left border point
    for i in range(1, n):
        if A[P[i]][0] < A[P[0]][0]:
            P[i], P[0] = P[0], P[i]
    # Sorting
    for i in range(2, n):
        j = i
        while j > 1 and (rotate(A[P[0]], A[P[j - 1]], A[P[j]]) < 0):
            P[j], P[j - 1] = P[j - 1], P[j]
            j -= 1
    # Border points
    S = [P[0], P[1]]
    for i in range(2, n):
        while rotate(A[S[-2]], A[S[-1]], A[P[i]]) < 0:
            del S[-1]  # pop(S)
        S.append(P[i])  # push(S,P[i])
    return S


class edge:
    """
    TEST PURPOSES
    """

    def __init__(self, argx1, argy1, argx2, argy2):
        self.x1 = argx1
        self.y1 = argy1
        self.x2 = argx2
        self.y2 = argy2
        self.length = ((self.x1 - self.x2) ** 2 + (
            self.y1 - self.y2) ** 2) ** 0.5
        self.a = (self.y2 - self.y1) / (self.x2 - self.x1)  # y=ax+b
        self.b = self.y1 - self.a * self.x1
        self.meanx = (self.x1 + self.x2) / 2
        self.meany = (self.y1 + self.y2) / 2

    def equals(self, edge):
        if ((self.x1 == edge.x1) and (self.y1 == edge.y1) and (
                self.x2 == edge.x2) and (self.y2 == edge.y2)):
            return True
        return False


def popmin3eges(list):
    """TEST PURPOSES"""
    minlength = list[0].length
    for i in range(0, len(list) - 1):
        if (list[i].length < minlength):
            minlength = list[i].length
    if i == 0:
        found1 = list[1]
        found2 = list[len(list) - 1]
        list.pop(i)
        list.remove(found1)
        list.remove(found2)
        return found1, found2
    if (i == len(list) - 1):
        found1 = list[len(list) - 2]
        found2 = list[0]
        list.pop(i)
        list.remove(found1)
        list.remove(found2)
        return found1, found2
    found1 = list[i - 1]
    found2 = list[i + 1]
    tmp = list.pop(i)
    list.remove(found1)
    list.remove(found2)
    print(found1.x1, found1.y1, found1.x2, found1.y2)
    print(tmp.x1, tmp.y1, tmp.x2, tmp.y2)
    print(found2.x1, found2.y1, found2.x2, found2.y2)
    return found1, found2


def isNear(a, b):
    """
    :return: True if two points are near, False otherwise
    """
    return True if abs(a - b) <= 25 else False

#
# # pre-process the image by resizing it, converting it to
# # graycale, blurring it, and computing an edge map
# # cap = cv2.VideoCapture(0)
# # ret, frame = cap.read()
# image = cv2.imread("original.jpg")
# # "C:/Users/Wovch/Documents/Edison/source/original.jpg")
# gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# blurred = cv2.GaussianBlur(gray, (5, 5), 0)
# edged = cv2.Canny(blurred, 50, 200, 255)
#
# # find contours in the edge map, then sort them by their
# # size in descending order
# cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,
#                         cv2.CHAIN_APPROX_SIMPLE)
# cnts = cnts[0] if imutils.is_cv2() else cnts[1]
# cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
# displayCnt = None
#
# # loop over the contours
# for c in cnts:
#     # approximate the contour
#     peri = cv2.arcLength(c, True)
#     approx = cv2.approxPolyDP(c, 0.02 * peri, True)
#
#     # if the contour has four vertices, then we have found
#     # the thermostat display
#     if len(approx) == 12:
#         displayCnt = approx
#         break
#
# # extract the thermostat display, apply a perspective transform
# # to it
# roadx = []
# roady = []
# for i in displayCnt:
#     roadx.append(i[0][0])
#     roady.append(i[0][1])
# pts = []
# for i in range(0, len(roadx)):
#     pts.append([roadx[i], roady[i]])
# convexNums = grahamscan(pts)
# xFirst = convexNums[0]
# # Getting firt half of the road
#
# firstHalfRoad = [point for point in convexNums if (isNear((pts[point])[0],
#  (pts[xFirst])[0]) \
#                                                    or isNear((pts[point])[
# 1], (pts[xFirst])[1])) and point != xFirst]
#
# firstRoad = [point for point in convexNums if (isNear((pts[point])[0],
# (pts[firstHalfRoad[0]])[0]) \
#                                                or isNear((pts[point])[1],
# (pts[firstHalfRoad[0]])[1])) and point !=
#              firstHalfRoad[0]] + firstHalfRoad
# # Other half of the road
# secondRoad = [i for i in set(convexNums) - set(firstRoad)]
#
# localApproxFirstRoad = [[[]]]
# localApproxSecondRoad = [[[]]]
# for y in firstRoad:
#     localApproxFirstRoad.append([[pts[y]]])
# localApproxFirstRoad.pop(0)
# for y in secondRoad:
#     localApproxSecondRoad.append([[pts[y]]])
# localApproxSecondRoad.pop(0)
# oigann = np.array(localApproxFirstRoad)
# r1 = four_point_transform(gray, oigann.reshape(4, 2))
# cv2.imwrite("pervert.jpg", r1)
# oigannn = np.array(localApproxSecondRoad)
# r2 = four_point_transform(gray, oigannn.reshape(4, 2))
# print(oigannn.reshape(4, 2))
# cv2.imwrite("pervert1.jpg", r2)
# mean = 0
# for i in r1:
#     for j in range(0, len(i)):
#         mean += i[j]
# mean = mean / (len(r1) - 1) / (len(r1[0]) - 1)
# blurred = cv2.GaussianBlur(r1, (11, 11), 0)
# thresh = cv2.threshold(blurred, mean*0.8, 255, cv2.THRESH_BINARY_INV)[1]
# thresh = cv2.erode(thresh, None, iterations=2)
# thresh = cv2.dilate(thresh, None, iterations=4)
# labels = measure.label(thresh, neighbors=8, background=0, return_num=True)
# print ((labels[1]))

# TEST CLASS
# currentRoad = timeAdvicer(file1="road1.jpg",file2="road2.jpg")
# currentRoad.test()
