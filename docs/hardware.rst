***********************************
2. Edison and hardware requirements
***********************************

.. _Edison and hardware requirements:

There is the brief list of required devices to install and use IoT Light Control.

* Intel Edison Compute Module
* Intel Edison Board
* Web-camera with the USB cable
* Estimote's Bluetooth low energy Beacon
* Grove - Buzzer
* Grove - Green LED
* Grove - Yellow LED
* Grove - Red LED
* SG90 9g Micro Servo 
* Mobile device under Android OS



Intel Edison Compute Module
===========================

The Intel® Edison Module is a tiny, SD-card-sized computing chip designed for building Internet of Things (IoT) and wearable computing products. The Edison module contains a high-speed, dual-core processing unit, integrated Wi-Fi*, Bluetooth* low energy, storage and memory, and a broad spectrum of input/output (I/O) options for interfacing with user systems. Because of its small footprint and low power consumption, the Edison module is an ideal choice for projects that need a lot of processing power without being connected to a power supply.
The Edison module is meant to be embedded in devices or development boards for connectivity and power options. To get started, Intel® provides the Intel® Edison Kit for Arduino* and Intel® Edison Breakout Board Kit*, which you can use for rapid prototyping. For production deployment, you can also create a custom board.
To program the Edison module, you can use the C, C++, Python*, or JavaScript* (Node.js*) programming language. To develop and debug the device code on Edison development boards or devices, download the integrated development environment (IDE) for your programming environment. For instance, you can download Intel® XDK for JavaScript, Intel® System Studio IoT Edition for C/C++, Intel® System Studio IoT Edition for Java, or the Arduino IDE for programming an Edison board with Arduino. The choice of IDE depends on your project and device requirements as well as which programming language you’ll use to interface with the devices.
To interact with sensors and actuators on Edison devices (or any supported device), Intel® provides the Libmraa* library. Libmraa provides an abstraction layer on top of supported hardware, so that you can read data from sensors and actuators in a standard way and create portable code that works across supported platforms. To check supported sensors and actuators from various manufacturers for Edison devices, browse the Useful Packages & Modules (UPM) Sensor/Actuator repository at GitHub* (https://github.com/intel-iot-devkit/upm). UPM is a high-level repository for various sensors, and provides a standard pattern for integrating with sensors using the Libmraa library. With the option of widely-used programming languages and a community of various sensor projects, you can reuse your existing programming knowledge to develop connected products, and use the Libmraa library to interact easily with GPIO pins for I/O functionality.
Visit the the official site for more details: https://software.intel.com/en-us/iot/hardware/edison

.. image:: pics/image15.png

.. image:: pics/image21.png

Intel Edison Board
==================


Arduino Breakout  essentially gives your Edison the ability to interface with Arduino shields or any board with the Arduino footprint. Digital pins 0 to 13 (and the adjacent AREF and GND pins), analog inputs 0 to 5, the power header, ICSP header, and the UART port pins (0 and 1) are all in the same locations as on the Arduino Uno R3 (Arduino 1.0 pinout). Additionally, the Intel® Edison Arduino Breakout includes a micro SD card connector, a micro USB device port connected to UART2, and a combination micro USB device connector and dedicated standard size USB 2.0 host Type-A connector (selectable via a mechanical microswitch). Though this kit won’t turn your Edison into an Arduino itself, you will, however, gain access to to the Arduino’s shield library and resources!

Board I/O Features:
20 digital input/output pins, including 6 pins as PWM outputs.
6 analog inputs.
1 UART (Rx/Tx).
1 I2C.
1 ICSP (In-system programming ) 6-pin header (SPI).
Micro USB device connector OR (via mechanical switch) dedicated standard size USB host Type-A connector.
Micro USB device (connected to UART).
SD card connector.
DC power jack (7 to 15VDC input).

.. image:: pics/image23.png

Grove kit
=========

Intel® Edison and Grove IoT Starter Kit Powered by AWS is a fully-integrated kit that includes Intel® Edison and Grove sensors and actuators with compiled and optimized AWS IoT SDK so developers and makers can build cloud-connected projects quickly! 

Designed for expert makers, entrepreneurs, and industrial IoT companies, the Intel® Edison module provides ease-of-development with a fully open source hardware and software development environment. It supports Wi-Fi and BLE 4.0 connectivity. Intel® Edison and Grove IoT Starter Kit Powered by AWS has included an Intel® Edison for Arduino along with 11 selective Grove sensors and actuators for user to detect the indoor environment as well as to create smart home applications.
GROVE is a family of plug and play open-source modules for easy and quick prototyping. Each Grove comes with standard interface, clear documentation and demo codes.

GROVE is an open modular toolset, designed to minimize the difficulty of fundamental electronic engineering. It is formed by functional modules TWIG and interface board STEM. Each TWIG has unified 4pin interface and standardized jigsaw shape for easy combination. They can work with major existing development platform (like Arduino and compatible boards, beagleboard, Xbee, and etc) via STEMs.


.. image:: pics/image14.png

.. image:: pics/image00.png


SG90 9g Micro Servo 
======================

.. image:: pics/image18.png


Tiny and lightweight with high output power. Servo can rotate approximately 180 degrees (90 in each direction), and works just like the standard kinds but smaller. You can use any servo code, hardware or library to control these servos. Good for beginners who want to make stuff move without building a motor controller with feedback & gear box, especially since it will fit in small places. It comes with a 3 horns (arms) and hardware. 

.. image:: pics/image19.png

For the project purposes The Servo library (Servo.py) has been used.
The Servo library (Servo.py) is used to control a servo motor attached to the Arduino Expansion Board for Edison.

.. image:: pics/image09.png

.. image:: pics/image04.png


Inside the micro servo, you will find the pieces from the above image. The top cover hosts the plastic gears while the middle cover hosts a DC motor, a controller, and the potentiometer.

.. image:: pics/image05.png


BLE
=======
Bluetooth low energy (Bluetooth LE, BLE, marketed as Bluetooth Smart) is a wireless personal area network technology designed and marketed by the Bluetooth Special Interest Group aimed at novel applications in the healthcare, fitness, beacons, security, and home entertainment industries. Compared to Classic Bluetooth, Bluetooth Smart is intended to provide considerably reduced power consumption and cost while maintaining a similar communication range.
Bluetooth Smart was originally introduced under the name Wibree by Nokia in 2006. It was merged into the main Bluetooth standard in 2010 with the adoption of the Bluetooth Core Specification Version 4.0.
Mobile operating systems including iOS, Android, Windows Phone and BlackBerry, as well as macOS, Linux, Windows 8 and Windows 10, natively support Bluetooth Smart. The Bluetooth SIG predicts that by 2018 more than 90 percent of Bluetooth-enabled smartphones will support Bluetooth Smart.
The Bluetooth SIG officially unveiled Bluetooth 5 on June 16, 2016 during a media event in London. One change on the marketing side is that they dropped the point number, so it now just called Bluetooth 5 (and not Bluetooth 5.0 or 5.0 LE like for Bluetooth 4.0). This decision was made allegedly to “simplifying marketing, and communicating user benefits more effectively”. On the technical side, Bluetooth 5 will quadruple the range, double the speed, and provide an eightfold increase in data broadcasting capacity of low energy Bluetooth transmissions compared to Bluetooth 4.x, which could be important for IoT applications where nodes are connected throughout a whole house.

.. image:: pics/image07.png

.. image:: pics/image22.jpg


