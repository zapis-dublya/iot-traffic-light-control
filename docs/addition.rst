******************
10. Additions
******************

.. _Additions:

Credits
=======


Development Team
----------------



* Vadim Andronov <vadimadr@gmail.com>
* Vladimir Zlobin <wovchena@gmail.com>
* Timofey Kuzmin <>
* Alexander Tsyplyaev <AlexTsyplyaev@gmail.com>
* Mihail Dolinin <mixaildolinin@gmail.com>

Any Contribution is welcome!

Contributing
============


Contributions are welcome, and they are greatly appreciated! Every
little bit helps, and credit will always be given.

You can contribute in many ways:

Types of Contributions
----------------------

Report Bugs
~~~~~~~~~~~

Report bugs at https://gitlab.com/zapis-dublya/iot-traffic-light-control/issues/issues.

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

Fix Bugs
~~~~~~~~

Look through the GitHub issues for bugs. Anything tagged with "bug"
and "help wanted" is open to whoever wants to implement it.

Implement Features
~~~~~~~~~~~~~~~~~~

Look through the GitHub issues for features. Anything tagged with "enhancement"
and "help wanted" is open to whoever wants to implement it.

Write Documentation
~~~~~~~~~~~~~~~~~~~

IoT Light Control could always use more documentation, whether as part of the
official IoT Light Control docs, in docstrings, or even on the web in blog posts,
articles, and such.

Submit Feedback
~~~~~~~~~~~~~~~

The best way to send feedback is to file an issue at https://gitlab.com/zapis-dublya/iot-traffic-light-control/issues/new

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

Get Started!
------------

Ready to contribute? Here's how to set up `iot_light_control` for local development.

1. Clone `iot_light_control` locally::

    $ git clone git@gitlab.com:zapis-dublya/iot-traffic-light-control.git

2. Install your local copy into a virtualenv. Assuming you have virtualenv installed, this is how you set up for local development::

    $ python3 -m virtualenv iot_light_control_env
    $ source iot_light_control_env/bin/activate
    $ cd iot_light_control/
    $ python setup.py develop

3. Install mraa and upm in mocking mode to prevent import errors:::

    $ git submodule update --init
    $ make iot-devkit

4. Create a branch for local development from dev branch::

    $ git checkout -b name-of-your-feature dev

   Now you can make your changes locally.

5. When you're done making changes, check that your changes pass flake8 and the tests, including testing other Python versions with tox::

    $ flake8 iot_light_control tests
    $ python setup.py test or pytest
    $ tox

   To get flake8 and tox, just pip install them into your virtualenv.

6. Commit your changes and push your branch to GitLab::

    $ git add .
    $ git commit -m "Your detailed description of your changes."
    $ git push origin name-of-your-bugfix-or-feature

7. Now merge your changes to dev without a fast-forward and push changes::

    $ git checkout dev
    $ git merge --no-ff name-of-your-feature
    $ git push origin dev


Tips
----

To run a subset of tests::

$ py.test tests.test_iot_light_control
