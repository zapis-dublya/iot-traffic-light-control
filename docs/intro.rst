***********************
1. The introduction
***********************

The IoT Light Control is the smart Traffic Light Controller based on Intel Edison. 
The IoT Light Control project was developed as the project for the Internet of Things course practice.

The main feature of the project is the `traffic light control <webcam.html>`__. This feature is implemented through opencv video-capture, and based on developed custom pattern recognition algorithm. The algorithm sets the most suitable time of the traffic light's life cycle.

As an additional feature, the barrier control though beacon technology is implemented. The barrier is being automatically lifted up when the transport with the beacon on the board arrives.

Besides of the automatic features listed above, the `manual control <mobile.html>`__ is available. Using the mobile device, the user gains the opportunity to control the system and to set his own desired parameters to be applied to the system.

The communication with the server is implemented through the REST API. The API is described in the `Server API <server.html>`__ part.

Any `contribution <contributing.html>`__ is welcome!

The `Zapis Dublya <authors.html>`__ team

