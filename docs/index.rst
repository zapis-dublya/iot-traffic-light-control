Welcome to IoT Light Control's documentation!
=============================================

.. toctree::
   :maxdepth: 2

   intro
   hardware
   software
   installation
   features
   webcam
   mobile
   server
   addition
   history
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
