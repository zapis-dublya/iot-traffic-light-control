*************
9. Server API
*************

.. _Server API:

There is the current implementation of public api methods.

.. code-block:: python
   
   api = Blueprint('api', url_prefix='api')
    
   def verify_user(request):
    ts = request.app.token_storage

    token = request.headers.get('X-Auth-Token')

    if not token:
        token = request.json['token']

    return ts[token]


   @api.route("/settings", methods=('GET', 'PUT'))
   async def settings(request):
    user = verify_user(request)
    app = request.app.main_app
    settings = app.settings.state
    if request.method == 'GET':
        return json({
            "settings": {
                "smartLight": settings['smart_light'],
                "barrierHeight": user.barrier_height,
                "greenDuration": settings['green_duration'],
                "redDuration": settings['red_duration']
            }
        })

    if request.method == 'PUT':
        new_settings = request.json

        user.barrier_height = new_settings['barrierHeight']
        user.save()

        settings['red_duration'] = new_settings['redDuration']
        settings['green_duration'] = new_settings['greenDuration']
        settings['smart_light'] = new_settings['smartLight']

        return json({"status": "ok"})


   @api.route("/test")
   async def api_test(request):
    return json({'status': 'ok'})


   def api_error(msg):
    return json({"error": msg}, 400)


   def create_token(user, ts):
    jwt = JWT(config.SECRET)
    timestamp = str(datetime.now().timestamp())
    token = jwt.dumps({'u': user.username, 't': timestamp}).decode()

    ts[token] = user

    Sessions.create(user=user, token=token)

    return json({"token": token})


   @api.route("/login", methods=('GET', 'POST'))
   async def api_login(request):
    token_storage = request.app.token_storage
    if request.method == 'GET':
        return json({"status": "ok"})

    if request.method == 'POST':
        username = request.json.get('username')
        password = request.json.get('password')

        if username is None or password is None:
            return api_error("Invalid data")

        password_hash = hashlib.sha1(password.encode()).hexdigest()

        s = User.select().where(User.username == username)

        if s.exists():
            user = s.first()

            if user.password_hash == password_hash:
                return create_token(user, token_storage)

        return api_error("Invalid user or password")


   @api.route("/register", methods=('POST',))
   async def api_login(request):
    username = request.json.get('username')
    password = request.json.get('password')

    if username is None or password is None:
        return api_error("Invalid data")

    if len(username) == 0:
        return api_error("Username is too short")

    if len(password) < 4:
        return api_error("Password should be longer than 4 symbols")

    s = User.select().where(User.username == username)

    if s.exists():
        return api_error("User already exists")

    password_hash = hashlib.sha1(password.encode()).hexdigest()

    new_user = User.create(username=username, password_hash=password_hash)

    return create_token(new_user, request.app.token_storage)


   @api.route('/barrier', methods=('GET', 'PUT'))
   async def barrier(request):
    app = request.app.main_app
    user = verify_user(request)
    barrier_open = app.state.state.get('barrier', 0) != 0
    if request.method == 'GET':
        return json({"barrierOpen": barrier_open})

    if request.method == 'PUT':
        if not barrier_open:
            app.state.state['barrier'] = user.barrier_height
        else:
            app.state.state['barrier'] = 0
        return json({"status": "ok"})

* PUT /api/settings

The API for changing the application settings. 

* GET /api/settings

The API that returns the application settings.

* GET /api/barrier

The API that returns the information whether the barrier is opened. 

* GET /api/test

The API for Edison search. Returns the information whether the application can access the Edison.

* PUT /api/barrier

The API that open the barrier. 

* GET /api/status

The API that returns the number of the autos on the crossroad. 

* GET /api/camera

The API that returns the image caught from the camera. 

* POST api/login 

The API for log a user in the application system.

* POST /api/register

The API that register the new user in the application system. 



