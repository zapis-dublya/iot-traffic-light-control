************************
5. Features and modules
************************

.. _Features and modules:

The main feature of the project is the traffic light control. This feature is implemented trough opencv video-capture, and based on developed custom pattern recognition algorithm. The algorithm sets the most suitable time of the traffic light's life cycle.

As an additional feature, the barrier control though beacon technology is implemented. The barrier is being automatically lifted up when the transport with the beacon on the board arrives.

Besides of the automatic features listed above, the manual control is available. Using the mobile device, the user gains the opportunity to control the system and to set his own desired parameters to be applied to the system.

These features and modules are described in following parts of the document.
