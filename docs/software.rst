***********************************************
3.Software requirements
***********************************************

.. _Software requirements:

OS Yocto
=================

The Yocto Project is an open source collaboration project that provides templates, tools and methods to help you create custom Linux-based systems for embedded products regardless of the hardware architecture. It was founded in 2010 as a collaboration among many hardware manufacturers, open-source operating systems vendors, and electronics companies to bring some order to the chaos of embedded Linux development.
As an open source project, the Yocto Project operates with a hierarchical governance structure based on meritocracy and managed by its chief architect, Richard Purdie, a Linux Foundation fellow. This enables the project to remain independent of any one of its member organizations, who participate in various ways and provide resources to the project.

The Yocto Project provides resources and information catering to both new and experienced users, and includes core system component recipes provided by the OpenEmbedded project. The Yocto Project also provides pointers to example code built demonstrating its capabilities. These community-tested images include the Yocto Project kernel and cover several build profiles across multiple architectures including ARM, PPC, MIPS, x86, and x86-64. Specific platform support takes the form of Board Support Package (BSP) layers for which a standard format has been developed. The project also provides an Eclipse IDE plug-in and a graphical user interface to the build system called Hob.

Here are some highlights for the Yocto Project:
Provides a recent Linux kernel along with a set of system commands and libraries suitable for the embedded environment.
Makes available system components such as X11, GTK+, Qt, Clutter, and SDL (among others) so you can create a rich user experience on devices that have display hardware. For devices that do not have a display or where you wish to use alternative UI frameworks, these components need not be installed.
Creates a focused and stable core compatible with the OpenEmbedded project with which you can easily and reliably build and develop.
Fully supports a wide range of hardware and device emulation through the Quick EMUlator (QEMU).
Provides a layer mechanism that allows you to easily extend the system, make customizations, and keep them organized.
You can use the Yocto Project to generate images for many kinds of devices. As mentioned earlier, the Yocto Project supports creation of reference images that you can boot within and emulate using QEMU. The standard example machines target QEMU full-system emulation for 32-bit and 64-bit variants of x86, ARM, MIPS, and PowerPC architectures. Beyond emulation, you can use the layer mechanism to extend support to just about any platform that Linux can run on and that a toolchain can target.
Another Yocto Project feature is the Sato reference User Interface. This optional UI that is based on GTK+ is intended for devices with restricted screen sizes and is included as part of the OpenEmbedded Core layer so that developers can test parts of the software stack.



Download the image: https://software.intel.com/en-us/iot/hardware/edison/downloads

.. image:: pics/image08.png



Important software
===================

You will need the following to build, install and use IoT Light Control:

* pip==9.0.1

* bumpversion==0.5.3

* wheel==0.29.0

* watchdog==0.8.3

* flake8==2.6.0

* tox==2.3.1

* coverage==4.1

* Sphinx==1.5.2

* pytest==2.9.2

* pytest-cov==2.4.0

* pytest-mock==1.5.0

To install these, 

.. code-block:: console

    $ git clone git@gitlab.com:zapis-dublya/iot-traffic-light-control.git

Then,

.. code-block:: console

    $ pip install -rrequirements_dev.txt


Ffmpeg 
========

ffmpeg 3.4.2
FFmpeg is the leading multimedia framework, able to decode, encode, transcode, mux, demux, stream, filter and play pretty much anything that humans and machines have created. It supports the most obscure ancient formats up to the cutting edge. No matter if they were designed by some standards committee, the community or a corporation. It is also highly portable: FFmpeg compiles, runs, and passes our testing infrastructure FATE across Linux, Mac OS X, Microsoft Windows, the BSDs, Solaris, etc. under a wide variety of build environments, machine architectures, and configurations. 
It contains libavcodec, libavutil, libavformat, libavfilter, libavdevice, libswscale and libswresample which can be used by applications. As well as ffmpeg, ffserver, ffplay and ffprobe which can be used by end users for transcoding, streaming and playing. 
The FFmpeg project tries to provide the best technically possible solution for developers of applications and end users alike. To achieve this we combine the best free software options available. We slightly favor our own code to keep the dependencies on other libs low and to maximize code sharing between parts of FFmpeg. Wherever the question of "best" cannot be answered we support both options so the end user can choose. 


Opencv 3.2
============

OpenCV is released under a BSD license and hence it’s free for both academic and commercial use. It has C++, C, Python and Java interfaces and supports Windows, Linux, Mac OS, iOS and Android. OpenCV was designed for computational efficiency and with a strong focus on real-time applications. Written in optimized C/C++, the library can take advantage of multi-core processing. Enabled with OpenCL, it can take advantage of the hardware acceleration of the underlying heterogeneous compute platform.
Adopted all around the world, OpenCV has more than 47 thousand people of user community and estimated number of downloads exceeding 14 million. Usage ranges from interactive art, to mines inspection, stitching maps on the web or through advanced robotics.
Required Packages
GCC 4.4.x or later
CMake 2.8.7 or higher
Git
GTK+2.x or higher, including headers (libgtk2.0-dev)
pkg-config
Python and Numpy with developer packages (python-dev, python-numpy)
ffmpeg or libav development packages: libavcodec-dev, libavformat-dev, libswscale-dev
[optional] libtbb2 libtbb-dev
[optional] libdc1394 2.x
[optional] libjpeg-dev, libpng-dev, libtiff-dev, libjasper-dev, libdc1394-22-dev
[optional] CUDA Toolkit 6.5 or higher
The packages can be installed using a terminal and the following commands or by using 
[Getting OpenCV Source Code
You can use the latest stable OpenCV version or you can grab the latest snapshot from Git repository.
Getting the Latest Stable OpenCV Version
Go to  downloads page.
Download the source archive and unpack it.
Getting the Cutting-edge OpenCV from the Git Repository
Launch Git client and clone OpenCV repository. 

.. code-block:: console

    cd ~/<my_working_directory>
    git clone https://github.com/opencv/opencv.git
    git clone https://github.com/opencv/opencv_contrib.git

Building OpenCV from Source Using CMake
Create a temporary directory, which we denote as <cmake_build_dir>, where you want to put the generated Makefiles, project files as well the object files and output binaries and enter there.
For example

.. code-block:: console

    cd ~/opencv
    mkdir build
    cd build

Configuring. Run cmake [<some optional parameters>] <path to the OpenCV source directory>
For example

.. code-block:: console

    cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local ..

or 

.. code-block:: console

    cmake-gui
    set full path to OpenCV source code, e.g. /home/user/opencv
    set full path to <cmake_build_dir>, e.g. /home/user/opencv/build
    set optional parameters

run: “Configure”
run: “Generate”
Note
Use 

.. code-block:: console

    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local .. ,

without spaces after -D if the above example doesn't work.
Description of some parameters
build type:

.. code-block:: console

    CMAKE_BUILD_TYPE=Release\Debug

to build with modules from opencv_contrib set

.. code-block:: console

    OPENCV_EXTRA_MODULES_PATH to <path to opencv_contrib/modules/>
    set BUILD_DOCS for building documents
    set BUILD_EXAMPLES to build all examples

[optional] Building python. Set the following python parameters:

.. code-block:: console

    
    PYTHON2(3)_EXECUTABLE = <path to python>
    PYTHON_INCLUDE_DIR = /usr/include/python<version>
    PYTHON_INCLUDE_DIR2 = /usr/include/x86_64-linux-gnu/python<version>
    PYTHON_LIBRARY = /usr/lib/x86_64-linux-gnu/libpython<version>.so
    PYTHON2(3)_NUMPY_INCLUDE_DIRS = /usr/lib/python<version>/dist-packages/numpy/core/include/

[optional] Building java.
Unset parameter: BUILD_SHARED_LIBS
It is useful also to unset BUILD_EXAMPLES, BUILD_TESTS, BUILD_PERF_TESTS - as they all will be statically linked with OpenCV and can take a lot of memory.
Build. From build directory execute make, it is recommended to do this in several threads
For example

.. code-block:: console

    make -j7 # runs 7 jobs in parallel

[optional] Building documents. Enter <cmake_build_dir/doc/> and run make with target "html_docs"
For example

.. code-block:: console

    cd ~/opencv/build/doc/
    make -j7 html_docs

To install libraries, execute the following command from build directory

.. code-block:: console

    sudo make install

[optional] Running tests
Get the required test data from OpenCV extra repository.
For example
git clone https://github.com/opencv/opencv_extra.git
set OPENCV_TEST_DATA_PATH environment variable to <path to opencv_extra/testdata>.
execute tests from build directory.
For example

.. code-block:: console

    <cmake_build_dir>/bin/opencv_test_core


UPM (Useful Packages & Modules) Sensor
===============================================================================================================
The UPM repository provides software drivers for a wide variety of commonly used sensors and actuators. These software drivers interact with the underlying hardware platform (or microcontroller), as well as with the attached sensors, through calls to MRAA APIs.
Programmers can access the interfaces for each sensor by including the sensor’s corresponding header file and instantiating the associated sensor class. In the typical use case, a constructor initializes the sensor based on parameters that identify the sensor, the I/O protocol used and the pin location of the sensor.
C++ interfaces have been defined for the following sensor/actuator types, but they are subject to change:
Light controller
Light sensor
Temperature sensor
Humidity sensor
Pressure sensor
Gas sensor
Analog to digital converter
The developer community is encouraged to help expand the list of supported sensors and actuators and provide feedback on interface design.
Supported Sensors
Supported sensor list from API documentation.
You can also refer to the Intel® IoT Developer Zone.
Installing UPM
Installing UPM 
UPM packages are provided for some of the major supported distributions, making it very easy to install UPM and its dependencies without having to go through a full build.
Poky Linux
Yocto Project based Poky Linux builds are provided for Intel Galileo, Intel Edison and Minnowboard. These are the official images released with the Intel IoT Developer Kit and can found here.
To update to the latest stable UPM version:
echo "src intel-iotdk https://iotdk.intel.com/repos/3.5/intelgalactic/opkg/i586/" > /etc/opkg/intel-iotdk.conf
opkg update
opkg upgrade mraa upm
If you would like to try the development version use intelgalactic-dev instead.
For more details visit this page.




libmraa - Low Level Skeleton Library for Communication on GNU/Linux platforms
==================================================================================================
Libmraa is a C/C++ library with bindings to Java, Python and JavaScript to interface with the IO on Galileo, Edison & other platforms, with a structured and sane API where port names/numbering matches the board that you are on. Use of libmraa does not tie you to specific hardware with board detection done at runtime you can create portable code that will work across the supported platforms.
The intent is to make it easier for developers and sensor manufacturers to map their sensors & actuators on top of supported hardware and to allow control of low level communication protocol by high level languages & constructs.
Installation:
Installing on Intel 32bit Yocto based opkg image
See the section below on compiling or use our repository to install on a glibc based yocto poky image that supports opkg. Adding this repository is as simple as and you'll have the latest stable tagged build of mraa installed!

.. code-block:: console

    echo "src mraa-upm http://iotdk.intel.com/repos/3.5/intelgalactic/opkg/i586" > /etc/opkg/mraa-upm.conf
    opkg update
    opkg install mraa

If you would like to get the latest & greatest builds from master HEAD you can use our -dev repository

.. code-block:: console
    
    echo "src mraa-upm http://iotdk.intel.com/repos/3.5/intelgalactic-dev/opkg/i586" > /etc/opkg/mraa-upm.conf
    opkg update
    opkg install mraa

Or Building:

Building libmraa {#building}
----------------------------
libmraa uses cmake in order to make compilation relatively painless. CMake runs build out of tree so the recommended way is to clone from git and make a build/ directory inside the clone directory.
For building imraa check building imraa
Build dependencies
Not all these are required but if you're unsure of what you're doing this is what you'll need:
* SWIG 3.0.5+
* git
* python 2.7 or 3.4+ (you'll need not just the interpreter but python-dev)
* node.js 4.x recommended (you'll need not just the interpreter but nodejs-dev)
* CMake 2.8.8+ (3.1+ is recommended for node.js version 2+)
* json-c 0.12+ (0.10+ probably works in reality)
For Debian-like distros the below command installs the basic set:

.. code-block:: console
    
    sudo apt-get install git build-essential swig3.0 python-dev nodejs-dev cmake libjson-c-dev

To build the documentation you'll also need:
* Doxygen 1.8.9.1+
* Graphviz 2+ (For Doxygen graph generation)
* Sphinx 1.1.3+ (For Python docs)
Basic build steps

.. code-block:: console
    
    mkdir build
    cd build
    cmake ..
    make

If this goes wrong and you have all the dependencies installed, then please file an issue with the full output of cmake .. and make or however far you got.
After that you can install built files (into default path) by running:

.. code-block:: console
    
    sudo make install

See flags for adjusting install paths in the section below.
Currently our install logic puts Python bindings into standard paths, which do not work on Debian due to their policy.
We are working on a permanent solution, in the meantime please use this command after make install to link installed modules where Debian's Python expects them:

.. code-block:: console
    
    sudo ln -s <your install prefix, e.g. /usr>/lib/python2.7/site-packages/* /usr/lib/python2.7/dist-packages

Same approach works for Python 3, you'll just need to adjust the version number in the path accordingly.
Configuration flags
Our CMake configuration has a number of options, cmake-gui or ccmake (cmake -i is no longer with us :() can show you all the options. A few of the more common ones are listed below. Note that when the option starts with CMAKE_ it's an option that is made available by CMake and will be similar in all CMake projects. You need to add them after cmake but before ..
A few recommended options:
Changing install path from /usr/local to /usr: 

.. code-block:: console
    
    -DCMAKE_INSTALL_PREFIX:PATH=/usr

Building debug build - adds -g and disables optimisations - this will force a full rebuild: 

.. code-block:: console
    
    -DCMAKE_BUILD_TYPE=DEBUG

Using clang instead of gcc: 

.. code-block:: console
    
    -DCMAKE_C_COMPILER=/usr/bin/clang -DCMAKE_CXX_COMPILER=/usr/bin/clang++

Building with an older version of SWIG (< 3.0.2) requires the disabling of JavaScript: 


.. code-block:: console
    
    -DBUILDSWIGNODE=OFF

Disabling Python module building: 

.. code-block:: console
    
    -DBUILDSWIGPYTHON=OFF

Building doc, this will require SPHINX & Doxygen: -DBUILDDOC=ON You will also require clone git submodules from your existing checkout: 

.. code-block:: console
    
    git submodule update --init --recursive The from doxygen2jsdoc dir: npm install mkdirp commander lodash bluebird pegjs

Override build architecture (this is useful because on x86 ARM code is not compiled so use this flag to force the target arch) 

.. code-block:: console
    
    -DBUILDARCH=arm

You can also enable -Wall for gcc before running cmake by exporting your wanted CC flags to the CC env var 

.. code-block:: console
    
    export CC="gcc -Wall"

Sometimes it's nice to build a static libary, on Linux systems just set  

.. code-block:: console
    
    -DBUILD_SHARED_LIBS=OFF

Dependencies continued
You'll need at least SWIG version 3.0.2 and we recommend 3.0.5 to build the JavaScript & Python modules. If your version of SWIG is older than this then please see above for disabling SWIGNODE. Otherwise you will get a weird build failure when building the JavaScript module. The Python module builds with SWIG 2.x but we don't test it.
During the build, we'll assume you're building from git, note that if you compile with git installed your version of mraa will be versioned with git desribe --tag to make it easy for intentification. You can easily modify version.c in build/src. If you don't build from a git tree then you will simply have a version which matches the latest released version of mraa.
Using a Yocto/OE toolchain
In order to compile with a Yocto/OE toolchain use the following toolchain file. This works well on the Edison 1.7.2 SDK. First source the environment file, then use our CMake toolchain file.
 

.. code-block:: console
    
    source /opt/poky-edison/1.7.2/environment-setup-core2-32-poky-linux
    mkdir build
    cmake -DCMAKE_TOOLCHAIN_FILE=../cmake/Toolchains/oe-sdk_cross.cmake ..
    make


Using Coverity
This is the procedure to submit a build to Coverity. You'll need to install coverity-submit for your OS.
 

.. code-block:: console
    
    mkdir covbuild/ && cd covbuild
    cmake -DBUILDDOC=OFF -DBUILDSWIG=OFF ..
    cov-build --dir cov-int make
    tar caf mraa.tar.bz2 cov-int


Building Java bindings
Have JAVA_HOME set to JDK install directory. Most distributions set this from /etc/profile.d/ and have a way of switching between alternatives. We support both OpenJDK and Oracle's JDK. On Arch Linux with OpenJDK 8 you'll have to set this yourself like this: 

.. code-block:: console
    
    export JAVA_HOME=/usr/lib/jvm/default/

Then use the CMake configuration flag: -DBUILDSWIGJAVA=ON To compile Example.java 

.. code-block:: console
    
    javac -cp $DIR_WHERE_YOU_INSTALLED_MRAA/mraa.jar:. Example.java


To run, make sure libmraajava.so is in LD_LIBRARY_PATH 

.. code-block:: console
    
    java -cp $DIR_WHERE_YOU_INSTALLED_MRAA/mraa.jar:. Example


If you want to add or improve Java bindings for mraa, please follow the Creating Java Bindings Guide.
Building an IPK/RPM package using cpack
You can get cpack to generate an IPK or RPM package fairly easily if you have the correct packaging tools 

.. code-block:: console
    
    cmake -DIPK=ON -DCMAKE_INSTALL_PREFIX=/usr ..
    make package


To use RPM simply enable the RPM option. You'll need rpmbuild installed on your build machine. 

.. code-block:: console
    
    cmake -DRPM=ON -DCMAKE_INSTALL_PREFIX=/usr ..

