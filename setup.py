#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'sanic',
    'sanic-cors',
    'requests',
    'itsdangerous',
    'peewee',
    'scikit-image',
    'imutils'
]

test_requirements = [
    'pytest',
    'coverage',
    'pytest-cov',
    'pytest-mock'

]

setup(
    name='iot_light_control',
    version='0.1.0',
    description="Smart Traffic Light Controller based on Inter Edison",
    long_description=readme + '\n\n' + history,
    author="Zapis Dublya Team",
    author_email='vadimadr@gmail.com',
    url='https://gitlab.com/zapis-dublya/iot-traffic-light-control',
    packages=[
        'iot_light_control',
    ],
    package_dir={
        'iot_light_control':
            'iot_light_control'
    },
    include_package_data=True,
    install_requires=requirements,
    license="MIT license",
    zip_safe=False,
    keywords='iot_light_control',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    entry_points={
        'console_scripts': ['iot_light_control=iot_light_control:main'],
    },
    test_suite='tests',
    tests_require=test_requirements,

)
