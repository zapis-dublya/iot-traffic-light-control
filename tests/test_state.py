from iot_light_control.state import State, StateStore


def test_state_created():
    state = State(None)
    assert state is not None
    state = State(None, {'a': 1})
    assert state is not None


def test_notified(mocker):
    store = StateStore()
    mocker.spy(store, 'notify')

    store.state['foo'] = 'bar'
    assert store.notify.call_count == 1
    store.state = {'foo': 'bar1'}
    assert store.notify.call_count == 2


def test_notified_once(mocker):
    store = StateStore()
    mocker.spy(store, 'notify')

    store.state['foo'] = 'bar'
    store.state['foo1'] = 'bar'
    store.state['foo'] = 'bar'
    assert store.notify.call_count == 2

    store = StateStore()
    mocker.spy(store, 'notify')

    store.state = {'foo': 'bar'}
    store.state = {'foo': 'bar', 'foo1': 'bar'}
    assert store.notify.call_count == 2


def test_observer():
    call_count = 0

    def observer(*args):
        nonlocal call_count
        call_count += 1

    store = StateStore()
    store.subscribe('foo', observer)
    store.state['foo'] = 'bar'
    store.state['foo1'] = 'bar'
    assert call_count == 1
