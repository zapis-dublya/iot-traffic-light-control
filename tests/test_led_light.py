# -*- coding: utf-8 -*-

from iot_light_control.led_light import LedLight


def test_led_light():
    led = LedLight(0)
    assert led.state is False
    led.toggle()
    assert led.state is True


def test_enable():
    led = LedLight(0)
    assert led.state is False
    led.enable()
    assert led.state is True
    led.enable()
    assert led.state is True


def test_disable():
    led = LedLight(0)
    assert led.state is False
    led.disable()
    assert led.state is False
