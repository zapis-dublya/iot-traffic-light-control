#!/usr/bin/env bash

# Installs mraa and upm in current virtual env

if [[ -z $VIRTUAL_ENV ]]; then
    echo 'You are not in virtualenv'
    exit 1
fi

if [[ -z $MRAA_DIR ]]; then
    [[ -d .build/mraa ]] && MRAA_DIR=.build/mraa
fi

if [[ -z $UPM_DIR ]]; then
    [[ -d .build/upm ]] && UPM_DIR=.build/upm
fi

SITEPACKEGES_DIR=`cd ${VIRTUAL_ENV}/lib/python3*/site-packages; pwd`
MRAA_PYTHON=${MRAA_DIR}/src/python/python3

cp ${MRAA_PYTHON}/mraa.py ${MRAA_PYTHON}/_mraa.so ${SITEPACKEGES_DIR}

mkdir -p ${SITEPACKEGES_DIR}/upm
touch ${SITEPACKEGES_DIR}/upm/__init__.py

find \( -path "./${UPM_DIR}/src/*/python3*/*.so" \
    -o -path "./${UPM_DIR}/src/*/python3*/*.py" \) \
    -exec cp {} ${SITEPACKEGES_DIR}/upm \;
