from flask import Flask, request, jsonify
from flask_cors import CORS
from pathlib import Path

app = Flask(__name__)
CORS(app)

if Path('edison_url').exists():
    with open('edison_url', 'r') as f:
        edison_url = f.read()
else:
    edison_url = ''


@app.route('/edison', methods=('GET', 'POST'))
def resolve():
    global edison_url
    if request.method == 'GET':
        return jsonify(edison_url=edison_url)
    else:
        data = request.json
        edison_url = data['url']
        with open('edison_url', 'w') as f:
            f.write(edison_url)
        return jsonify(status='ok')


if __name__ == '__main__':
    app.run('0.0.0.0', '5050')
