#!/usr/bin/env bash

# Builds mraa and upm

mkdir -p .build/mraa .build/upm

# Build mraa

cd .build/mraa
cmake ../../libs/mraa -DBUILDARCH="MOCK" -DBUILDSWIGNODE=OFF
make -j4

# Build upm

cd ../upm
cmake ../../libs/upm -DBUILDSWIGPYTHON=ON -DBUILDEXAMPLES=OFF -DBUILDSWIGNODE=OFF -DBUILDSWIGJAVA=OFF
make -j4
